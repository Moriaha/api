<?php
class Controllers_Mult extends RestController {
	public function get() {
		if (isset($this->request['params']['a'])&&isset($this->request['params']['b'])){
			$result = $this->request['params']['a']*$this->request['params']['b'];
			$this->response = array('result' =>$result );
			$this->responseStatus = 200;
		}else{
			$this->response = array('result' =>'Wrong parameters for Mult' );
			$this->responseStatus = 200;			
		}
	}
	public function post() {
		$this->response = array('result' => 'no post implemented for Mult');
		$this->responseStatus = 201;
	}
	public function put() {
		$this->response = array('result' => 'no put implemented for Mult');
		$this->responseStatus = 200;
	}
	public function delete() {
		$this->response = array('result' => 'no delete implemented for Mult');
		$this->responseStatus = 200;
	}
}
