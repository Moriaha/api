<?php
class Controllers_Users extends RestController {

	private $_con;

	private function DBconnect(){
	//	$this->_con=mysqli_connect("localhost","ronih_test1_a","test1_a","ronih_test1");
		$this->_con=mysqli_connect("localhost","moriaha","47369","moriaha_REAT_API");
	}
	
	public function get() {
		$this->DBconnect();
		//var_dump($this->request['params']['id']);
		//die();
		if (isset($this->request['params']['id'])){
			if(!empty($this->request['params']['id'])) {
				$sql="SELECT * FROM users WHERE id=?";				
				$stmt = $this->_con->prepare($sql);
				$stmt->bind_param("s",$this->request['params']['id']);
				$stmt->execute();
				$stmt->store_result();				
				$stmt->bind_result($id, $name,$email);
				$stmt->fetch();
				$result = ['id'=>$id,'name'=>$name,'email'=>$email];
				$this->response = array('result' =>$result );
				$this->responseStatus = 200;
			} else { //ID does not exist in the URL - output all users
				$sql ="SELECT * FROM users";
				$usersSqlResult=mysqli_query($this->_con, $sql);
				$users=[];
				while ($user = mysqli_fetch_array($usersSqlResult)){
					$users[]= ['name' => $user['name'], 'email'=> $user['email']];
				}
				$this->response = array('result' =>$users);  //rest.php file will convert the array to Json in the function jsonResponse()
				$this->responseStatus = 200; // message of success 
			}	
		}else{ 
			$this->response = array('result' =>'Wrong parameters for get users' );
			$this->responseStatus = 200;			
		}
	}
	
	public function post() {
		$this->DBconnect();
	if (isset($this->request['params']['payload']) and empty($this->request['params']['id'])){
		$json = json_decode($this->request['params']['payload'],true);
		$name= $json['name'];
		$email= $json['email'];
		$sql ="INSERT INTO users (name, email) VALUES ('$name','$email')";
		$stmt = $this->_con->prepare($sql);
		$stmt->execute();
		$last_id = $this->_con->insert_id;
		$result = ['id'=>$last_id,'name'=>$name,'email'=>$email];
		$this->response = array('result' =>$result );
		$this->responseStatus = 200;

		}else{ 
			$this->response = array('result' =>'Wrong parameters for create new users' );
			$this->responseStatus = 200;			
		}
	}
	
	public function put() {
	//	$this->response = array('result' => 'no put implemented for users');
	//	$this->responseStatus = 200;
	$this->DBconnect();
		if (isset($this->request['params']['payload']) and isset($this->request['params']['id']) and !empty($this->request['params']['id'])){
		
		$json = json_decode($this->request['params']['payload'],true);
			$id1=$this->request['params']['id'];
			$result=[];
			$result['id'] = $id1;
			if(array_key_exists("name",$json)){
				$name= $json['name'];
				$sql ="UPDATE users SET name='$name' WHERE id='$id1'";
				$stmt = $this->_con->prepare($sql);
				$stmt->execute();
			$result['name'] = $name;
			}
			if(array_key_exists("email",$json)){
				$email= $json['email'];
				$sql ="UPDATE users SET email='$email' WHERE id='$id1'";
				$stmt = $this->_con->prepare($sql);
				$stmt->execute();
			$result['email'] = $email;
			}
			$this->response = array('result' =>$result );
			$this->responseStatus = 200;

		}else{ 
			$this->response = array('result' =>'Wrong parameters for update user' );
			$this->responseStatus = 200;			
		}
	
	}
	
	public function delete() {
	//	$this->response = array('result' =>'no delete implemented for users' );
	//	$this->responseStatus = 200;	
		$this->DBconnect();
		if (isset($this->request['params']['id']) and !empty($this->request['params']['id'])){
				$id1=$this->request['params']['id'];
				$sql="DELETE FROM users WHERE id='$id1'";				
				$stmt = $this->_con->prepare($sql);
				$stmt->execute();
				$this->response = array('result' =>"user ".$this->request['params']['id'] ." have deleted" );
				$this->responseStatus = 200; 	
		}else{ 
			$this->response = array('result' =>'Wrong parameters for delete user' );
			$this->responseStatus = 200;			
		}
	}
}
